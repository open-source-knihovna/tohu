Tohu
====

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/60ef750ded6a422488b003e9d4fda0bd)](https://www.codacy.com/app/xmorave2/tohu?utm_source=github.com&utm_medium=referral&utm_content=open-source-knihovna/tohu&utm_campaign=badger)
[![Dependency Status](https://www.versioneye.com/user/projects/58b7475b9fd69a003c585995/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/58b7475b9fd69a003c585995)

Main goal of this project is build prototype of self-check station for libraries, software and hardware based on open architecture and code.

Read more on wiki: https://github.com/open-source-knihovna/tohu/wiki

Installation & running
----------------------
For easy deployment there is prepared Docker image

1. Install docker and docker-compose
2. Clone this repository
3. Run ./install.sh
4. Run 'sudo docker-compose up --build'
5. Go to http://localhost:8082 to test
